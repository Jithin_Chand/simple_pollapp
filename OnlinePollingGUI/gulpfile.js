var gulp  = require('gulp'),
    gutil = require('gulp-util');
    liveReload = require('browser-sync').create();

// Live reload when some file is saved.
gulp.task('liveReload', function() {
    liveReload.init({
        port: 8080,
        ui: {
            port: 8585,
        },
        server: {
            baseDir: '',
            index: 'index.html'
        },
    })
});

gulp.task('serve', ['liveReload'], function (){
    gulp.watch('*.html', liveReload.reload);
    gulp.watch('app.js', liveReload.reload);
    gulp.watch('*.css', liveReload.reload);
    gulp.watch('poll/*.html', liveReload.reload);
    gulp.watch('poll/*.js', liveReload.reload);
    gulp.watch('poll/*.css', liveReload.reload);
    gulp.watch('events/*.html', liveReload.reload);
    gulp.watch('events/*.js', liveReload.reload);
    gulp.watch('events/*.css', liveReload.reload);
});