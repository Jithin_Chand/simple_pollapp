angular
    .module('eventsList')
    .factory('eventsListFactory', function ($http, HOST_DOMAIN) {
        return {
            //get all the events available from node.
            getEventsList: function () {
                return $http.get(HOST_DOMAIN+'/index').then(function (res) {
                    return res.data;
                });
            }
        }
    })
    .controller('EventsCtrl', function ($scope, $http, eventList) {
        'use strict';
        $scope.listOfEvents = eventList;
        console.log($scope.listOfEvents);
    });