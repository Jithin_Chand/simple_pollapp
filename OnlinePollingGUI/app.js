angular.module('gui', ['ngRoute', 'ngResource', 'eventsList', 'newPoll'])
    .constant('HOST_DOMAIN', 'http://localhost:8888')
    .config(function ($routeProvider) {
        'use strict';

        // routes
        $routeProvider
            .when('/events', {
                controller : 'EventsCtrl',
                templateUrl: '/events/events.html',
                resolve: {
                    eventList: function (eventsListFactory) {
                        return eventsListFactory.getEventsList();
                    }
                }
            })
            .when('/poll', {
                controller : 'PollCtrl',
                templateUrl: 'poll/poll.html',
                resolve: {
                    newPoll: function (newPollFactory) {
                        return newPollFactory.getNewPoll();
                    }
                }
            })
            .otherwise({
                redirectTo : 'index.html'
            });
    })
    .controller('appCtrl', function ($scope, $location) {
        // active tab in navigation bar
       $scope.isActive = function (currentPath) {
           return currentPath == $location.path();
       };
    });