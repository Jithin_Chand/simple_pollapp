angular
    .module('newPoll')
    .factory('newPollFactory', function ($http, HOST_DOMAIN) {
        return {
            // generates a random number and gets specific event from node
            getNewPoll: function () {
                var eventId = Math.floor((Math.random() * 18) + 1);
                return $http.get(HOST_DOMAIN+'/poll?group='+eventId).then(function (res) {
                    return res.data;
                });
            }
        }
    })
    .controller('PollCtrl', function ($scope, $http, newPoll) {
        'use strict';
        $scope.eventNotFinished = true;
        $scope.newPoll = newPoll;
        // if the state of the event is finished then no poll for this event.
        if($scope.newPoll.state == 'FINISHED') {
            $scope.eventNotFinished = false;
        }
        $scope.hideRadioBtns = true;
        $scope.radioBtnsData = data();
        function data() {
            // draw option for only football
            if (newPoll.sport == "FOOTBALL") {
                return [
                    {label:'1', value: newPoll.homeName},
                    {label:'2', value: "Draw"},
                    {label:'3', value: newPoll.awayName}
                ];
            }
            else {
                return [
                    {label:'1', value: newPoll.homeName},
                    {label:'2', value: newPoll.awayName}
                ];
            }
        };
        $scope.selectedResponse = function () {
            $scope.hideRadioBtns = false;
        };
        console.log($scope.newPoll);
    });