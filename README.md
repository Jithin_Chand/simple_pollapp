# README #

### Execution ###
#Backend#

* Goto OnlinePollingSystem
* Run $npm install
* Run $node app.js

#Backend - Testcases#

* In a new terminal run $npm test

#Frontend#

* Run $npm install
* Run $bower install
* Run $gulp serve
* Visit localhost:8080

### Tools used ###

* Intellij
* nodejs
* angularjs
* bower
* gulp
* nedb

### Description ###

* Developed backend in nodejs with two endpoints. One for fetching all the
  events and the another for fetching a single event.
* Whenever backend is started it fetches all the events from the given JSON
  file to nedb local datastore.
* Developed frontend in angularjs.
* In GUI, events page displays all the events.
* Whenever you navigate to polls page or click the next button, it generates 
  a random number between 1-18 and sends it to the backend for fetching the 
  corresponding event.

### Additional information ###

* I had spent a total of around 8 hours on the task.
* I had just started to learn unit testing in angularjs, so I have written any
  test cases in frontend.
* The GUI is pretty simple and basic. It might take more time for me to make it
  look muck better.

