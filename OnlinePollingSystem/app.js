var express = require('express');
var path = require('path');
var fs = require('fs');
var favicon = require('serve-favicon');
var PORT = 8888;

var Datastore = require('nedb');
// Persistent datastore with automatic loading
var db = new Datastore({ filename: './data/finalScheduleDB.json', autoload: true });

var app = express();

// Additional headers to avoid cross origin errors.
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:8080");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
  next();
});

// index route to get all events
app.get('/index', function (req, res) {
  db.find({}, function (err, docs) {
    if (err) {
      res.json(500, err);
      return;
    }
    res.json(docs);
  });
});

// poll route to get specific event for polls
app.get('/poll', function (req, res) {
  var id = req.query.group;
  db.find({}, function (err, docs) {
    if (err) {
      res.json(500, err);
      return;
    }
    res.json(docs[id - 1]);
  });
});

// inserting data into nedb file
function insertInDb(message, res) {
  db.insert(message, function (err, newDoc) {
    if (err) {
      res.json(500, err);
      return;
    }
  });
}

app.listen(PORT, function () {
  var json = JSON.parse(fs.readFileSync('./data/test-assignment.json', 'utf8'));
  for (var obj in json) {
    insertInDb(json[obj]);
  }
  console.log('Started app on port 8888!');
});

// deleting nedb file when process stopped.
process.on ('SIGINT', function() {
  fs.unlink('./data/finalScheduleDB.json',function(err){
    if(err) return console.log(err);
  });
  console.log("Process stopped by pressing [CTRL-C]");
  process.exit(0);
});

module.exports = app;
