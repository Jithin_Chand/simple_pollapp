var expect  = require("chai").expect;
var request = require("request");
var PORT = 8888;

describe ("Testing Rest end points", function () {
    describe("Testing index endpoint", function () {
        var url = "http://localhost:"+PORT+"/index";
        it ("returns status 200", function (done) {
            request (url, function (err, res, body) {
                expect(res.statusCode).to.equal(200);
                done();
            });
        });
        it ("returns 18 records", function (done) {
            request (url, function (err, res, body) {
                expect(JSON.parse(body).length).to.equal(18);
                done();
            });
        });

    });
    describe("Testing poll endpoint", function () {
        var url = "http://localhost:"+PORT+"/poll?group=1";
        it ("returns status 200", function (done) {
           request(url, function (err, res, body) {
               expect(res.statusCode).to.equal(200);
               done();
           })
        });
        it ("testing 1st records sport", function (done) {
            request (url, function (err, res, body) {
                expect(JSON.parse(body).sport).to.equal('HANDBALL');
                done();
            });
        });
        it ("testing 1st records name", function (done) {
            request (url, function (err, res, body) {
                expect(JSON.parse(body).name).to.equal('Dinamo Astrakhan - SKIF-Krasnodar');
                done();
            });
        });
    });
});